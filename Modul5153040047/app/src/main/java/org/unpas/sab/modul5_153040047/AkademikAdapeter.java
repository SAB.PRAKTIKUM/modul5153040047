package org.unpas.sab.modul5_153040047;

import android.media.Image;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by admin601 on 4/13/2018.
 */

public class AkademikAdapeter extends RecyclerView.Adapter<AkademikAdapeter.ViewHolder> {
    private ArrayList<HashMap<String, String>> postList;
    private MainActivity activity;
    public AkademikAdapeter(ArrayList<HashMap<String, String>>postList,
                            MainActivity mainActivity){
        this.postList = postList;
        this.activity = mainActivity;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_akademik,
                viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        final HashMap<String, String> post = postList.get(position);
        Glide.with(activity).load(post.get("img_url")).into(viewHolder.imgLogo);
        viewHolder.textSingaktan.setText(post.get("singkatan"));
        viewHolder.textNama.setText(post.get("nama"));
        viewHolder.textUrl.setText(post.get("url"));
    }

    @Override
    public int getItemCount() {
        return postList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        ImageView imgLogo;
        TextView textSingaktan;
        TextView textNama;
        TextView textUrl;

        public ViewHolder(View view){
            super(view);
            imgLogo = (ImageView) view.findViewById(R.id.imgLogo);
            textSingaktan = (TextView) view.findViewById(R.id.textSingkatan);
            textNama = (TextView) view.findViewById(R.id.textNama);
            textUrl = (TextView) view.findViewById(R.id.textUrl);

        }
    }
}
